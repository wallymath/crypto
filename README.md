This is a easy package demonstrating linear attack on SPN cypher
Test System:
Ubuntu 12.04 LTS 3.13.0-39-generic
To perform linear attack, first run SPN encryption
./SPN [plain text] [SPN config] [key] [number of time]
where plain text is the a 2 byte data to be encrypt, type "random" to input random plain text
SPN config is the config storing the S-box and permutation
key is the original key used for encyption, tpye "random" to use random key
number of time is the # of plain-text cypher-text pair create to the stdout


e.g.
./SPN rand SPN.cfg key.dat 10000 > data 2>log

the used key and round keys will be printed in the head of stderr (log)

Then run linear atk on the stored plain-cypher pair

./linear_atk SPN.cfg data
it will output the 8 bits of the last round key
