#include<cstdio>
#include<iostream>
#include<bitset>
#include<stdint.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>


inline void sbox(uint8_t* val, int* s){
    uint8_t ret[2]={0};
    ret[0] |= (s[val[0]>>4]<<4) + (s[val[0]&15]);
    ret[1] |= (s[val[1]>>4]<<4) + (s[val[1]&15]);
    memcpy(val, ret, 2);
}

inline void permute(uint8_t* val, int* p){
    uint8_t ret[2]={0};
    for(int i=0;i<8;i++)
        if(p[i]>7&&(val[0]&(1<<(7-i))))
            ret[1] += 1 << (15-p[i]);
        else if (p[i]<=7 && val[0]&(1<<(7-i)))
            ret[0] += 1 << (7-p[i]);
    for(int i=8;i<16;i++)
        if(p[i]>7&&(val[1]&(1<<(15-i))))
            ret[1] += 1 << (15-p[i]);
        else if (p[i]<=7 && val[1]&(1<<(15-i)))
            ret[0] += 1 << (7-p[i]);
    memcpy(val, ret, 2);
}

int main(int argc, char* argv[]){
    if(argc != 5){
        fprintf(stderr, "./SPN [plain text] [SPN config] [key config] [times]\n");
        return -1;
    }
    srand(time(NULL));
    int s[16], p[16];
    uint8_t rk[5][2];
    int key;
    uint8_t plain[2], tmp;
    uint8_t mid_val[2];

    FILE* fp_spn = fopen(argv[2], "r");
    for(int i=0;i<16; i++)
        fscanf(fp_spn,"%d",&s[i]);
    for(int i=0;i<16; i++)
        fscanf(fp_spn,"%d",&p[i]);
    fclose(fp_spn);
    if(strcmp(argv[3], "rand")==0){
        for(int i=0;i<4;i++)
            key = rand();
        if(rand()&1)
            key= ~key;
    }
    else{
        FILE* fp_key = fopen(argv[3], "r");
        fscanf(fp_key, "%d",&key);
        fclose(fp_key);
    }
    std::cerr << "key = " << std::bitset<32>(key) << std::endl;
    for(int i=0;i<5;i++){
        memcpy(rk[i], &key ,2);
        tmp = rk[i][0];    //swap rk because of big endian
        rk[i][0] = rk[i][1];
        rk[i][1] = tmp;
        key >>= 4;
        std::cerr << "rk[" << i <<"] = " << std::bitset<8>(rk[i][0]) << std::bitset<8>(rk[i][1]) << std::endl;
    }
    for(int j=0;j<atoi(argv[4]);j++){
        if(strcmp(argv[1], "rand")==0){
            plain[0] = rand()&255;
            plain[1] = rand()&255;
        }
        else
            memcpy(plain, argv[1], strlen(argv[1]));
        memcpy(mid_val, plain, 2);
        // start encrpytion
        for(int i=0; i<4;i++){
            // Add Round Key
            //        std::cerr << "mid[" << i <<"] = " << std::bitset<8>(mid_val[0]);
            //        std::cerr << std::bitset<8>(mid_val[1]) << std::endl;
            mid_val[0] ^= rk[i][0];
            mid_val[1] ^= rk[i][1];
            //        std::cerr << "midr[" << i <<"] = " << std::bitset<8>(mid_val[0]);
            //        std::cerr << std::bitset<8>(mid_val[1]) << std::endl;
            sbox(mid_val, s);
            //        std::cerr << "mids[" << i <<"] = " << std::bitset<8>(mid_val[0]);
            //        std::cerr << std::bitset<8>(mid_val[1]) << std::endl;
            if(i!=3)
                permute(mid_val, p); 
        }
        //Add RK[4]
        mid_val[0] ^= rk[4][0];
        mid_val[1] ^= rk[4][1];
        std::cerr << "plain : " << std::bitset<8>(plain[0]) << std::bitset<8>(plain[1]) << std::endl;
        std::cerr << "cipher : " << std::bitset<8>(mid_val[0]) << std::bitset<8>(mid_val[1]) << std::endl;
        std::cout << std::bitset<8>(plain[0]) << std::bitset<8>(plain[1]) << " ";
        std::cout << std::bitset<8>(mid_val[0]) << std::bitset<8>(mid_val[1]) << std::endl;
    }
    return 0;
}

