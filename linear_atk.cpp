#include<cstdio>
#include<iostream>
#include<vector>
#include<bitset>
#include<stdint.h>
typedef struct dat{
    uint8_t plain[2];
    uint8_t cypher[2];
    dat(char* a, char* b){
        for(int i=0;i<2;i++){
            plain[i]=0, cypher[i]=0;
        }
        for(int i=0;i<8;i++){
            if(a[i]=='1') plain[0] += 1<<(7-i);
            if(b[i]=='1') cypher[0] += 1<<(7-i);
        }
        for(int i=8;i<16;i++){
            if(a[i]=='1') plain[1] += 1<<(15-i);
            if(b[i]=='1') cypher[1] += 1<<(15-i);
        }
    }
    void printb(){
        std::cerr << std::bitset<8>(plain[0]) << std::bitset<8>(plain[1]) << " " << std::bitset<8>(cypher[0]) << std::bitset<8>(cypher[1]) << std::endl;
    }
} DAT;

inline int check(DAT data, uint8_t u2, uint8_t u4){
    //extract X5^X7^X8^u2_2^u2_4^u4_2^u4_4
    int ret=0;
    if(data.plain[0]&(1<<3)) ret = !ret;
    if(data.plain[0]&(1<<1)) ret = !ret;
    if(data.plain[0]&1) ret = !ret;
    if(u2&(1<<2)) ret = !ret;
    if(u2&1) ret = !ret;
    if(u4&(1<<2)) ret = !ret;
    if(u4&1) ret = !ret;
    return ret;
}

int main(int argc, char* argv[]){
    if(argc!=3){
        fprintf(stderr, "./linear_atk [SPN config] [data]\n");
        return 0;
    }
    FILE* fp_spn = fopen(argv[1], "r");
    int s[16], inv_s[16];
    for(int i=0;i<16;i++){
        fscanf(fp_spn, "%d", &s[i]);
        inv_s[s[i]]=i;
    }
    FILE* fp = fopen(argv[2], "r");
    
    char a[16], b[16];
    int cnt=0, check_cnt=0;
    int count[16][16]={};
    uint8_t u2,u4;
    while(fscanf(fp, "%s%s", a,b)!=EOF){
        DAT data(a,b);
        for(int i=0;i<16;i++)
            for(int j=0;j<16;j++){
                u2 = inv_s[i ^ (data.cypher[0]&15)];
                u4 = inv_s[j ^ (data.cypher[1]&15)];
                if(check(data, u2, u4)){
                    check_cnt++;
                    count[i][j]++;
                }
            }
        cnt++;
    }
    int max=0, max_i, max_j;
    for(int i=0;i<16;i++)
        for(int j=0;j<16;j++){
            if(count[i][j] > cnt/2)
                count[i][j] -= cnt/2;
            else
                count[i][j] = cnt/2-count[i][j];
            if(count[i][j]>max){
                max=count[i][j];
                max_i=i; max_j=j;
            }
        }
    
    std::cout << "K2=" << std::bitset<4>(max_i) << ", K4=" << std::bitset<4>(max_j) <<", ratio = " << (double)max/cnt<<std::endl;
}
